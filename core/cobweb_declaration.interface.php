<?php
/**
 * @version $Id: cobweb_declaration.interface.php 236 2009-07-14 16:07:22Z oystein.rg $
 * @licence http://www.opensource.org/licenses/bsd-license.php The BSD License
 * @copyright Upstruct Berlin Oslo
 */

/**
 * @package Cobweb
 * @subpackage Core
 * @author Øystein Riiser Gundersen <oystein@upstruct.com>
 * @version $Revision: 236 $
 * @needsdocumentation
 */
interface CobwebDeclaration {
	public function configuration();
	public function createConfiguration(array $configuration);
	
	public function request();
	public function createRequest(array $configuration);
	
	public function dispatcher();
	public function createDispatcher(array $configuration);
	
	public function resolver();
	public function createResolver(array $configuration);
	
	public function applicationManager();
	public function createApplicationManager(array $configuration);
	
	public function middlewareManager();
	public function createMiddlewareManager(array $configuration);
}

?>