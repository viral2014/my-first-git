<?php
/**
 * @version $Id: function.cobweb_reverse_url_map.php 276 2009-08-03 08:04:22Z oystein.rg $
 * @licence http://www.opensource.org/licenses/bsd-license.php The BSD License
 * @copyright Upstruct Berlin Oslo
 */

/**
 * @author     Øystein Riiser Gundersen
 * @package    Cobweb
 * @subpackage Templating
 * @version    $Rev: 276 $
 */
function smarty_function_cobweb_reverse_url_map($parameters, &$smarty) {

	$map = array();
	$reverse = Cobweb::get('__RESOLVER__')->reverseMap();
	foreach ($reverse as $a => $p)
		$map[$a] = preg_replace('/(\()(\?P?<\w+>)(.*?\))/', '$1$3', $p);

	return JSON::encode($map);
	
}