<?php
/**
 * @version $Id: function.url.php 171 2009-07-07 12:57:33Z oystein $
 * @licence http://www.opensource.org/licenses/bsd-license.php The BSD License
 * @copyright Upstruct Berlin Oslo
 */


function smarty_function_url($parameters, &$smarty) {
	return Cobweb::get('__RESOLVER__')->reverse($parameters['name'], array_slice($parameters, 1));
}