<?php
/**
 * @version $Id: modifier.floatformat.php 171 2009-07-07 12:57:33Z oystein $
 * @licence http://www.opensource.org/licenses/bsd-license.php The BSD License
 * @copyright Upstruct Berlin Oslo
 */

function smarty_modifier_floatformat($string, $decimals = 0) {
	return sprintf("%.{$decimals}f", floatval($string));
	
}