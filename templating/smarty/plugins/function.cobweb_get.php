<?php

/**
 * @version $Id: function.cobweb_get.php 384 2009-11-20 09:54:01Z oystein.rg $
 * @licence http://www.opensource.org/licenses/bsd-license.php The BSD License
 * @copyright Upstruct Berlin Oslo
 */


function  smarty_function_cobweb_get($parameters, &$smarty) {
	return Cobweb::get($parameters['key']);
}