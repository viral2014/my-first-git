<?php
/**
 * @version $Id: smarty_template_adapter.class.php 391 2009-12-06 12:46:49Z oystein.rg $
 * @licence http://www.opensource.org/licenses/bsd-license.php The BSD License
 * @copyright Upstruct Berlin Oslo
 */

require_once dirname(__FILE__) . '/smarty_template.class.php';

/**
 * @package Cobweb
 * @subpackage Templating
 */
class SmartyTemplateAdapter extends TemplateAdapter {
	
	private $smarty;
	
	public function __construct() {
		$this->smarty = new SmartyTemplate();
		parent::__construct();
	}
	
	public function interpolate($template, $interpolation_mode = TemplateAdapter::INTERPOLATE_FILE) {
		$this->smarty->assign($this->bindings());
		return $this->smarty->renderFile($template);
	}
	
	
}