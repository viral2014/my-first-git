<?php
/**
 * @version $Id: user_group_permission.model.php 293 2009-08-06 08:50:26Z oystein.rg $
 * @licence http://www.opensource.org/licenses/bsd-license.php The BSD License
 * @copyright Upstruct Berlin Oslo
 */

/**
 * @author     Øystein Riiser Gundersen
 * @package    Cobweb
 * @subpackage Authentification
 * @version    $Revision: 293 $
 */
class UsergroupPermission extends Doctrine_Record {
	
	public function setTableDefinition() {
		$this->hasColumn('permission_id', 'integer', NULL, array('primary' => true));
        $this->hasColumn('usergroup_id', 'integer', NULL, array('primary' => true));
    }	
}
