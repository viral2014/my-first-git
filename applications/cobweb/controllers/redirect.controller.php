<?php
/**
 * @version $Id: redirect.controller.php 151 2009-07-07 09:15:52Z oystein $
 * @licence http://www.opensource.org/licenses/bsd-license.php The BSD License
 * @copyright Upstruct Berlin Oslo
 */

/**
 * @author     Øystein Riiser Gundersen <oystein@upstruct.com>
 * @version    $Rev: 151 $
 * @package    Cobweb
 * @subpackage Cobweb Application
 */
class RedirectController {
	
	public function to($url, $permanent = true) {
		if (!$url) return new HTTPResponseGone();
		
		return $permanent ?
			new HTTPResponsePermanentRedirect($url) :
			new HTTPResonseRedirect($url);
	}
	
	public function toAction($label, array $arguments = array()) {
		return parent::redirect('@' . $label, $arguments);
	}
	
}