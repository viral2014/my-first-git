<?php
/**
 * @version $Id: request_processor.interface.php 280 2009-08-03 11:28:14Z oystein.rg $
 * @licence http://www.opensource.org/licenses/bsd-license.php The BSD License
 * @copyright Upstruct Berlin Oslo
 */

/**
 * @author     Øystein Riiser Gundersen <oystein@upstruct.com>
 * @package    Cobweb
 * @subpackage Middleware
 * @version    $Revision: 280 $
 */
interface RequestProcessor {
	public function processRequest(Request $request);
	public function processResponse(Request $request, Response $response);
	public function processAction(Request $request, Action $action);
	public function processException(Request $request, Exception $exception);
}
