<?php // index.php
define('COBWEB_DIRECTORY', 'D:\xampp7.1.9\htdocs\cobweb');
define('COBWEB_PROJECT_DIRECTORY', 'D:\xampp7.1.9\htdocs\cobweb');
define('COBWEB_WWW_ROOT', COBWEB_PROJECT_DIRECTORY . '\\');

require_once COBWEB_DIRECTORY . '\core\cobweb_bootstrap.inc.php';

Cobweb::start();