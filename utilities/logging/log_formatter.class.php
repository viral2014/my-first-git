<?php
/**
 * @version $Id: log_formatter.class.php 356 2009-11-09 12:44:38Z oystein.rg $
 * @licence http://www.opensource.org/licenses/bsd-license.php The BSD License
 * @copyright Upstruct Berlin Oslo
 */

/**
 * @package Cobweb
 * @subpackage Logging
 */
abstract class LogFormatter {
	
	protected $logger;
	
	public function __construct(Logger $logger) {
		$this->logger = $logger;
	}
	
	abstract public function format(Response $response);
	
}