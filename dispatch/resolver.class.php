<?php
/**
 * @version $Id: resolver.class.php 379 2009-11-16 14:16:10Z oystein.rg $
 * @licence http://www.opensource.org/licenses/bsd-license.php The BSD License
 * @copyright Upstruct Berlin Oslo
 */

/**
 * @package Cobweb
 * @subpackage Dispatch
 */
interface Resolver {
	
	/**
	 * @param  Request $request
	 * @return Action
	 */
	public function resolve(Request $request);
	
}